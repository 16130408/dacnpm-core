package com.bezkoder.springjwt.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.bezkoder.springjwt.models.News;
import com.bezkoder.springjwt.models.NewsComments;

public interface CommentsRepository extends CrudRepository<NewsComments, Long> {

	List<NewsComments> findBynews(News news);

	List<NewsComments> findByNewsOrderByIdAsc(News newDTO);

}
