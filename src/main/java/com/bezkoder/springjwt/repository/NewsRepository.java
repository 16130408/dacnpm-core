package com.bezkoder.springjwt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bezkoder.springjwt.models.News;

@Repository
public interface NewsRepository extends CrudRepository<News, Long> {
	List<News> findAllByOrderByIdDesc();

	Optional<News> findById(Long id);
	
	@Query(value = "SELECT e.* FROM news e WHERE e.user_id = :userid order by id DESC", nativeQuery = true)
	List<News> findByUserId(@Param("userid") Long user_id);
	
}
