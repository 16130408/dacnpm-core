package com.bezkoder.springjwt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.models.News;
import com.bezkoder.springjwt.models.NewsLiked;

@Service
public class LikeService {
	@Autowired
	private LikeRepository likeRepository;

	@Autowired
	private NewsRepository newsRepository;

	public List<NewsLiked> findById(Long id) {
		Optional<News> news = newsRepository.findById(id);
		News newDTO = new News(news.get().getId(), news.get().getTitle(), news.get().getContent(),
				news.get().getImage(), news.get().getLikes(), news.get().getCreatedDate(), news.get().getStatus());
		List<NewsLiked> newsComment = likeRepository.findBynews(newDTO);
		return newsComment;
	}
}
