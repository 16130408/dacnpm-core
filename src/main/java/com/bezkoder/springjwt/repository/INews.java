package com.bezkoder.springjwt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.bezkoder.springjwt.models.News;

public interface INews {
	List<News> findAll();

	Optional<News> findById(Long id);

	List<News> findByCreatedBy(Long user_id);

	boolean createNews(News news, MultipartFile[] file, String username);

	News findByIdNews(Long id);
}
