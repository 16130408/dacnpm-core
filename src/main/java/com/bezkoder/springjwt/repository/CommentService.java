package com.bezkoder.springjwt.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.DTO.NewsCommentsDTO;
import com.bezkoder.springjwt.models.News;
import com.bezkoder.springjwt.models.NewsComments;

@Service
public class CommentService {

	@Autowired
	private CommentsRepository commentsRepository;

	@Autowired
	private NewsRepository newsRepository;

	public List<NewsCommentsDTO> findById(Long id) {
		Optional<News> news = newsRepository.findById(id);
		System.out.println(news);
		News newDTO = new News(news.get().getId(), news.get().getTitle(), news.get().getContent(),
				news.get().getImage(), news.get().getLikes(), news.get().getCreatedDate(), news.get().getStatus());
		List<NewsComments> newsComment = commentsRepository.findByNewsOrderByIdAsc(newDTO);
		List<NewsCommentsDTO> newsCommentDTO = new ArrayList<NewsCommentsDTO>();
		for (NewsComments comments : newsComment) {
			NewsCommentsDTO dto = new NewsCommentsDTO(comments.getId(), comments.getNews().getId(),
					comments.getComment(), comments.getSentiment(), comments.getUser().getUsername());
			newsCommentDTO.add(dto);
		}
		return newsCommentDTO;
	}
}
