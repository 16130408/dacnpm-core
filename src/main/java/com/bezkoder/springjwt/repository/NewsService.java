package com.bezkoder.springjwt.repository;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bezkoder.springjwt.models.News;
import com.bezkoder.springjwt.models.User;

@Service
public class NewsService implements INews {

	@Autowired
	private NewsRepository newsRepository;

	@Autowired
	private UserRepository userRepository;

	static HttpServletRequest request;

	@Override
	public List<News> findAll() {
		List<News> news = newsRepository.findAllByOrderByIdDesc();
		return news;
	}

	@Override
	public Optional<News> findById(Long id) {
		Optional<News> news = newsRepository.findById(id);
		return news;
	}

	public static ArrayList<String> uploadFile(MultipartFile[] images) {
		String rootPath = "data/images/";
		String src = "src/main/webapp/";
		File uploadDir = new File(src + rootPath);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}
		MultipartFile[] fileDatas = images;
		ArrayList<String> nameFile = new ArrayList<String>();
		for (MultipartFile file : fileDatas) {
			String name = file.getOriginalFilename();
			if (name != null && name.length() > 0) {
				try {
					String[] nameImg = name.split("\\.");
					name = UUID.randomUUID().toString() + "." + nameImg[nameImg.length - 1];
					File fileServer = new File(uploadDir.getAbsolutePath() + File.separator + name);
					BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(fileServer));
					out.write(file.getBytes());
					out.close();
					nameFile.add(rootPath + name);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return nameFile;
	}

	@Override
	public boolean createNews(News news, MultipartFile[] file, String username) {
		User user = userRepository.findByusername(username);
		LocalDateTime time = LocalDateTime.now();
		if (user != null) {
			if (file.length == 0) {
				News info = new News(news.getTitle(), news.getContent(), time, true, user);
				newsRepository.save(info);
			} else {
				ArrayList<String> images = uploadFile(file);
				for (String image : images) {
					News info = new News(news.getTitle(), news.getContent(), image, time, true, user);
					newsRepository.save(info);
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public List<News> findByCreatedBy(Long user_id) {
		List<News> newsByUserId = newsRepository.findByUserId(user_id);
		return newsByUserId;
	}

	@Override
	public News findByIdNews(Long id) {
		Optional<News> news = newsRepository.findById(id);
		if (!news.isPresent()) {
			News newsDTO = new News(news.get().getId(), news.get().getTitle(), news.get().getContent(),
					news.get().getImage(), news.get().getLikes(), news.get().getCreatedDate(), news.get().getStatus());
			return null;
		}
		News newsDTO = new News(news.get().getId(), news.get().getTitle(), news.get().getContent(),
				news.get().getImage(), news.get().getLikes(), news.get().getCreatedDate(), news.get().getStatus());
		return newsDTO;
	}

}
