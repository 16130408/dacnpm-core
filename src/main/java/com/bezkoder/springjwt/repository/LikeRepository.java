package com.bezkoder.springjwt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bezkoder.springjwt.models.News;
import com.bezkoder.springjwt.models.NewsLiked;
import com.bezkoder.springjwt.models.User;

@Repository
public interface LikeRepository extends CrudRepository<NewsLiked, Long> {
	List<NewsLiked> findBynews(News news);

	Optional<NewsLiked> findByUserAndNews(User user, News news);
}
