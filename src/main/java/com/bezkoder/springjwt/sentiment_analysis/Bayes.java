package com.bezkoder.springjwt.sentiment_analysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Bayes {
	HashMap<String, Integer> mapPos;
	HashMap<String, Integer> mapNeg;
	ArrayList<String> wordsPos;
	ArrayList<String> wordsNeg;
	int posTotal = 0;
	int negTotal = 0;

	// Hàm trả về trạng thái
	public String sentiment(double[] probs) throws Exception {
		String answer = "";
		if (probs[0] > probs[1])
			answer = "positive";
		if (probs[1] > probs[0])
			answer = "negative";
		if (probs[0] > .45 && probs[0] < .55)
			answer = "neutral";
		return answer;
	}

	// Hàm tính xác xuất
	public double[] calcProbs(String[] words) {
		double[] probs = new double[2];
		double probPos = ((double) wordsPos.size() / (wordsPos.size() + wordsNeg.size()));
		double probNeg = ((double) wordsNeg.size() / (wordsPos.size() + wordsNeg.size()));
		for (String word : words) {
			if (mapPos.containsKey(word)) {
				probPos *= (double) (mapPos.get(word) + 1.0) / (posTotal + mapPos.size());
			} else
				probPos *= (double) (1.0) / (posTotal + mapPos.size());
			if (mapNeg.containsKey(word))
				probNeg *= (double) (mapNeg.get(word) + 1.0) / (negTotal + mapNeg.size());
			else
				probNeg *= (double) (1.0) / (negTotal + mapNeg.size());
		}
		System.out.println(mapPos.size());
		probs[0] = (probPos / (probPos + probNeg));
		probs[1] = (probNeg / (probPos + probNeg));
		for (String word : words) {
			if ((word.equals("not") || word.equals("don't")) && probs[0] - probs[1] > 0.05) {
				probs[0] = 0;
				probs[1] = 1;
			} else if ((word.equals("not") || word.equals("don't")) && probs[1] - probs[0] > 0.05) {
				probs[0] = 1;
				probs[1] = 0;
			}

		}
		return probs;
	}

	// Load từ từ output dô map. 1 key với weight = 1;
	public void loadHashMap() throws Exception {
		mapPos = new HashMap<>();
		wordsPos = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader("posOutput.txt"));
		String line = "";
		while ((line = br.readLine()) != null) {
			wordsPos.add(line);
			String[] words = line.split(" ");
			for (String key : words) {
				posTotal++;
				if (mapPos.containsKey(key)) {
					mapPos.put(key, mapPos.get(key) + 1);
				} else {
					mapPos.put(key, 1);
				}
			}
		}
		br.close();

		mapNeg = new HashMap<>();
		wordsNeg = new ArrayList<>();
		BufferedReader br2 = new BufferedReader(new FileReader("negOutput.txt"));
		while ((line = br2.readLine()) != null) {
			wordsNeg.add(line);
			String[] words = line.split(" ");
			for (String key : words) {
				negTotal++;
				if (mapNeg.containsKey(key)) {
					mapNeg.put(key, mapNeg.get(key) + 1);
				} else {
					mapNeg.put(key, 1);
				}
			}
		}
		br2.close();

	}

}
