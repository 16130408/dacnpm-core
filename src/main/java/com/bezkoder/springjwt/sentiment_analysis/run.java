package com.bezkoder.springjwt.sentiment_analysis;
import java.io.File;
import java.util.ArrayList;

public class run {

	public static  String checkComment(String comment) throws Exception {
		Preprocessing p = new Preprocessing();
		// đọc 3 file training
		File pos = new File("positiveTraining.txt");
		File neg = new File("negativeTraining.txt");
		ArrayList<ArrayList<String>> positiveList = p.readFile(pos);
		ArrayList<ArrayList<String>> negativeList = p.readFile(neg);

		p.positiveList = positiveList;
		p.negativeList = negativeList;
		// gọi đến hàm void tfidf trong Preprocessing
		p.tfidf(positiveList, 1);
		p.tfidf(negativeList, -1);
		// gọi hàm loadfile ở Preorocessing
		p.loadFile();
		// thuật toán Naive Bayes
		Bayes bayes = new Bayes();
		bayes.loadHashMap();
		// Thuật toán LogisticRegression
		LogisticRegression lr = new LogisticRegression();
		lr.loadHashMap();
		runPreprocess run = new runPreprocess();
		
		run.testing(bayes, comment);
		//run.testing(lr, p);
		return run.testing(lr, comment);

	}

}
