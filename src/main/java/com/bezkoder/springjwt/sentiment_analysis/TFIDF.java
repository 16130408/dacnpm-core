package com.bezkoder.springjwt.sentiment_analysis;
import java.util.ArrayList;

public class TFIDF {
	// tần số xuất hiện của 1 từ trong 1 văn bản
	public double tf(ArrayList<ArrayList<String>> words, String word) throws Exception {

		double count = 0;
		for (ArrayList<String> line : words) {
			for (String w : line) {
				if (word.equalsIgnoreCase(w)) {
					count++;
				}
			}
		}
		return ((double) count / words.size());
		// số lần xuất hiện từ chia cho số lần xuất hiện nhiều nhất của một từ bất kỳ trong văn bản.
	}

	// Tần số nghịch của 1 từ trong tập văn bản đánh giá tầm quan trọng của 1 từ
	public double idf(ArrayList<ArrayList<String>> words, String word) throws Exception {
		double count = 0;
		for (ArrayList<String> list : words) {
			if (list.contains(word)) {
				count++;
			}
		}
		return Math.log((double) words.size() / count);                                          
	}

	// Hàm tính từ có TFIDF càng cao thì từ đó xuất hiện nhiều trong văn bản
	public double tfidf(ArrayList<ArrayList<String>> words, String w) throws Exception {
		return tf(words, w) * idf(words, w);
	}

}
