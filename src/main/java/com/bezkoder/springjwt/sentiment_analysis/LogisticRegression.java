package com.bezkoder.springjwt.sentiment_analysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;

public class LogisticRegression {
	HashMap<String, Integer> mapPos;
	HashMap<String, Integer> mapNeg;
	boolean negation = false; // Phủ định
	boolean intensifier = false; // Khằng định
	int posTotal = 0;
	int negTotal = 0;

	// hàm in ra sentiment
	public String sentiment(double[] probs) throws Exception {
		String answer = "neutral";
		if (probs[0] > probs[1])
			answer = "positive";
		if (probs[1] > probs[0])
			answer = "negative";
		if (probs[0] > .45 && probs[0] < .55)
			answer = "neutral";
		return answer;
	}

	// Hàm tính toán
	public double[] calcProbs(String[] words) throws Exception { // Nhận vô 1
																	// dòng
		double[] probs = new double[2];
		int posSum = 0;
		int negSum = 0;
		for (String word : words) {
			word = word.toLowerCase();
			posSum += sumOfWeight(word, 1);
			negSum += sumOfWeight(word, -1);
			if (word.equals("not") || word.equals("don't")) {
				negation = true;
			}
			if (word.equals("very") || word.equals("really")) {
				intensifier = true;
			}
		}
		probs[0] = (double) posSum / (posSum + negSum);
		probs[1] = (double) negSum / (posSum + negSum);
//		System.out.println(negSum);
//		System.out.println(posSum);
//		System.out.println(probs[1]);
		if (posSum < 0) {
			probs[0] = 0;
			probs[1] = 1;
		} else if (negSum < 0) {
			probs[1] = 0;
			probs[0] = 1;
		}
		return probs;
	}

	// Tính giá trị của từ đó xuất hiện trong câu
	public int sumOfWeight(String word, int emotion) {
		int weight = 1;

		if (emotion == -1) {
			if (mapNeg.containsKey(word)) {
				weight = mapNeg.get(word);
				if (negation) {
					weight *= -1;
					negation = false;
				}
				if (intensifier) {
					weight *= 2;
					intensifier = false;
				}
			}
		} else if (emotion == 1) {
			if (mapPos.containsKey(word)) {
				weight = mapPos.get(word);
				if (negation) {
					weight *= -1;
					negation = false;
				}
				if (intensifier) {
					weight *= 2;
					intensifier = false;
				}
			}
		}
		return weight;
	}

	// Load vào map
	public void loadHashMap() throws Exception {
		mapPos = new HashMap<>();

		BufferedReader br = new BufferedReader(new FileReader("posFeatures.txt"));
		String line = "";
		while ((line = br.readLine()) != null) {
			String[] words = line.split(" ");
			posTotal += Math.pow(Integer.parseInt(words[1]), 2);
			// System.out.println(posTotal);
			mapPos.put(words[0], Integer.parseInt(words[1]));
		}
		br.close();

		mapNeg = new HashMap<>();
		BufferedReader br2 = new BufferedReader(new FileReader("negFeatures.txt"));
		while ((line = br2.readLine()) != null) {
			String[] words = line.split(" ");
			negTotal += Math.pow(Integer.parseInt(words[1]), 2);
			// System.out.println(negTotal);
			mapNeg.put(words[0], Integer.parseInt(words[1]));
		}
		br2.close();
	}

}
