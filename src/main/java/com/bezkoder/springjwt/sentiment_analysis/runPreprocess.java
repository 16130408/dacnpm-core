package com.bezkoder.springjwt.sentiment_analysis;

public class runPreprocess {
	public String testing(Bayes bayes, String comment) throws Exception {
		String rec = "";
		rec = comment;
		String[] words = rec.split(" ");
		double[] probs = bayes.calcProbs(words);
		String answer = bayes.sentiment(probs);
		// System.out.printf("%s | Sentiment: %s Positive: %.2f Negative: %.2f\n", rec,
		// answer, probs[0], probs[1]);
		return answer;

	}

	public String testing(LogisticRegression lr, String comment) throws Exception {
		String rec = "";
		rec = comment;
		String[] words = rec.split(" ");
		double[] probs = lr.calcProbs(words);
		String answer = lr.sentiment(probs);
		return answer;

	}

}
