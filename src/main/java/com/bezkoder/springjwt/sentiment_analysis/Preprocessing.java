package com.bezkoder.springjwt.sentiment_analysis;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Preprocessing {
	
	public ArrayList<ArrayList<String>> positiveList;
	public ArrayList<ArrayList<String>> negativeList;

	public ArrayList<ArrayList<String>> posOutput = new ArrayList<>();
	public ArrayList<ArrayList<String>> negOutput = new ArrayList<>();
//	Phương thức đọc file từ lớp training lên (đọc theo từng dòng)
	public ArrayList<ArrayList<String>> readFile( File f ) throws Exception {
		ArrayList<ArrayList<String>> words = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(f));
		String rec;
		while ( (rec = br.readLine() ) != null ) {
			ArrayList<String> data = new ArrayList<>();
			String[] line = rec.split(" ");
			for(String word : line)
				data.add(word);
			words.add(data);
		}
		
		br.close();
		
		return words;
	}
//	Đưa data từ file training vào các list để chuẩn bị đưa vào output để đánh giá
	public void tfidf(ArrayList<ArrayList<String>> words, int emotion) throws Exception {
		
		double threshold = .3;
		TFIDF tfidf = new TFIDF();
		for(ArrayList<String> list : words) {
			ArrayList<String> passed = new ArrayList<>();
			for(String w : list) {
				double weight = tfidf.tfidf(words, w);				// Giá trị TFIDF của từ
//				System.out.println(w + " " + weight + " " + emotion);
				if(threshold > weight && !w.equals(""))
					passed.add(w);		
			}

			if(!passed.isEmpty() && emotion == 1 ) {
				posOutput.add(passed);
			}
			if(!passed.isEmpty() && emotion == -1 ) {
				negOutput.add(passed);
			}
		}
	}
	
//	ghi dữ liệu vào file Output
	public void loadFile() throws IOException {
		BufferedWriter bw  = new BufferedWriter(new FileWriter("posOutput.txt"));
		BufferedWriter bw1 = new BufferedWriter(new FileWriter("negOutput.txt"));
		

		for(ArrayList<String> words : posOutput) {
			for( String w : words ) { 
				bw.write(w + " ");
			}
			bw.newLine();
		}
		for(ArrayList<String> words : negOutput) {
			for( String w : words ) { 
				bw1.write(w + " ");
			}
			bw1.newLine();
		}
		bw.flush();
		bw.close();
		
		bw1.flush();
		bw1.close();
		
		
		
	}

}
