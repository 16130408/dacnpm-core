package com.bezkoder.springjwt.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "news_comment")
public class NewsComments {

	@Id
	@GeneratedValue
	private int id;

	@Size(max = 255)
	private String comment;

	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	private News news;
	
	private String sentiment;

	public NewsComments() {
		super();
	}

	public NewsComments(int id, User user, News news, String comment, String sentiment) {
		this.id = id;
		this.user = user;
		this.news = news;
		this.comment = comment;
		this.sentiment = sentiment;
	}

	public NewsComments(@Size(max = 255) String comment, User user, News news, String sentiment) {
		this.comment = comment;
		this.user = user;
		this.news = news;
		this.sentiment = sentiment;
	}

	
	
	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "NewsComments [id=" + id + ", user=" + user + ", news=" + news + "]";
	}

}
