package com.bezkoder.springjwt.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

//@Entity
//@Table(name = "room")
public class Room {

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
//	
//	@NotBlank
//	@Size(max = 50)
//	@Column(name = "roomname")
//	private String roomname;
//	
//	@Column(name = "description")
//	private String description;
//	
//	@Column(name = "createby")
//	private String createBy;
//	
//	@Column(name = "location")
//	private String location;
//	
//	
//	@ManyToMany(fetch = FetchType.LAZY)
//	@JoinTable(	name = "user_room", 
//				joinColumns = @JoinColumn(name = "room_id"), 
//				inverseJoinColumns = @JoinColumn(name = "user_id"))
//	private Set<User> users = new HashSet<>();
//	
//	
//	
//	public Room() {
//		
//	}
//
//	public Room(String roomname, String description, String createBy, String location) {
//		this.roomname = roomname;
//		this.description = description;
//		this.createBy = createBy;
//		this.location = location;
//	}
//	
//	
//
//	public String getLocation() {
//		return location;
//	}
//
//	public void setLocation(String location) {
//		this.location = location;
//	}
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getRoomname() {
//		return roomname;
//	}
//
//	public void setRoomname(String roomname) {
//		this.roomname = roomname;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	public void setDescription(String description) {
//		this.description = description;
//	}
//
//	public String getCreateBy() {
//		return createBy;
//	}
//
//	public void setCreateBy(String createBy) {
//		this.createBy = createBy;
//	}
//
//	public Set<User> getUsers() {
//		return users;
//	}
//
//	public void setUsers(Set<User> users) {
//		this.users = users;
//	}
//
//	@Override
//	public String toString() {
//		return "Room [id=" + id + ", roomname=" + roomname + ", description=" + description + ", createBy=" + createBy
//				+ ", users=" + users + "]";
//	}
//	
}
