package com.bezkoder.springjwt.models;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

@Entity
@Table(name = "news")
public class News {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 50)
	@Column(name = "title")
	private String title;

	@Column(name = "content")
	private String content;

	@Column(name = "image")
	private String image;

	@Column(name = "likes")
	private int likes;

	@Column(name = "created_date")
	private LocalDateTime createdDate;

	@Column(columnDefinition = "boolean default true")
	private boolean status;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User createdBy;

	@OneToMany(mappedBy = "news")
	private List<NewsLiked> liked;

	@OneToMany(mappedBy = "news")
	private List<NewsSaved> saved;

	@OneToMany(mappedBy = "news")
	private List<NewsComments> comment;

	public News() {

	}

	public News(@NotBlank @Size(max = 50) String title, String content, String image, LocalDateTime createdDate,
			boolean status, User createdBy) {
		this.title = title;
		this.content = content;
		this.image = image;
		this.createdDate = createdDate;
		this.status = status;
		this.createdBy = createdBy;
	}

	public News(Long id, @NotBlank @Size(max = 50) String title, String content, String image, int likes,
			LocalDateTime createdDate, boolean status) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.image = image;
		this.likes = likes;
		this.createdDate = createdDate;
		this.status = status;
	}

	public News(@NotBlank @Size(max = 50) String title, String content) {
		this.title = title;
		this.content = content;
	}

	public News(@NotBlank @Size(max = 50) String title, String content, LocalDateTime createdDate, boolean status,
			User createdBy) {
		this.title = title;
		this.content = content;
		this.createdDate = createdDate;
		this.status = status;
		this.createdBy = createdBy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", content=" + content + ", image=" + image + ", likes=" + likes
				+ ", createdDate=" + createdDate + ", status=" + status + ", createdBy=" + createdBy + "]";
	}

}
