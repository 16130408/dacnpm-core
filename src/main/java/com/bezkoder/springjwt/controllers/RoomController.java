package com.bezkoder.springjwt.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.springjwt.models.Room;
import com.bezkoder.springjwt.payload.response.MessageResponse;
import com.bezkoder.springjwt.repository.RoomRepository;
//@CrossOrigin(origins = "*", maxAge = 3600)
//@RestController
//@RequestMapping("/api")
public class RoomController {

//	@Autowired
//	RoomRepository roomRepository;

//	@GetMapping("/room")
//	public ResponseEntity<List<Room>> getAllRoom(@RequestParam(required = false) String roomname) {
//		try {
//			List<Room> rooms = new ArrayList<Room>();
//
//			if (roomname == null)
//				roomRepository.findAll().forEach(rooms::add);
//			else
//				roomRepository.findByRoomnameContaining(roomname).forEach(rooms::add);
//
//			if (rooms.isEmpty()) {
//				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//			}
//			return new ResponseEntity<>(rooms, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//	
//	 @GetMapping("/room/{id}")
//	  public ResponseEntity<Room> getRoomById(@PathVariable("id") long id) {
//	    Optional<Room> roomData = roomRepository.findById(id);
//
//	    if (roomData.isPresent()) {
//	      return new ResponseEntity<>(roomData.get(), HttpStatus.OK);
//	    } else {
//	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//	    }
//	  }
//
////	@PostMapping("/room")
////	public ResponseEntity<Room> createRoom(@RequestBody Room room) {
////		try {
////			Room _room = roomRepository.save(new Room(room.getRoomname(), room.getDescription(), room.getCreateBy()));
////			return new ResponseEntity<>(_room, HttpStatus.CREATED);
////		} catch (Exception e) {
////			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
////		}
////	}
//	@PostMapping("/room")
//	public ResponseEntity<?> createRoom(@Valid @RequestBody Room room) {
//		if (roomRepository.existsByRoomname(room.getRoomname())) {
//			return ResponseEntity.badRequest().body(new MessageResponse("Error: Room is already exist!"));
//		}
//		Room _room = new Room(room.getRoomname(), room.getDescription(), room.getCreateBy(), room.getLocation());
//		roomRepository.save(_room);
//		return ResponseEntity.ok(new MessageResponse("Room created successfully!"));
//	}
//	
//	
//	
//	
//	
//	 @PutMapping("/room/{id}")
//	  public ResponseEntity<Room> updateTutorial(@PathVariable("id") long id, @RequestBody Room room) {
//	    Optional<Room> roomData = roomRepository.findById(id);
//
//	    if (roomData.isPresent()) {
//	      Room _room = roomData.get();
//	      _room.setRoomname(room.getRoomname());
//	      _room.setDescription(room.getDescription());
//	      _room.setCreateBy(room.getCreateBy());
//	      return new ResponseEntity<>(roomRepository.save(_room), HttpStatus.OK);
//	    } else {
//	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//	    }
//	  }
//
//	  @DeleteMapping("/room/{id}")
//	  public ResponseEntity<HttpStatus> deleteRoom(@PathVariable("id") long id) {
//	    try {
//	      roomRepository.deleteById(id);
//	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//	    } catch (Exception e) {
//	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
//	    }
//	  }
//
//	  @DeleteMapping("/room")
//	  public ResponseEntity<HttpStatus> deleteAllRoom() {
//	    try {
//	      roomRepository.deleteAll();
//	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//	    } catch (Exception e) {
//	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
//	    }
//
//	  }
//	
}
