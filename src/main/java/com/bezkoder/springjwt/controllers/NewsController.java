package com.bezkoder.springjwt.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bezkoder.springjwt.DTO.NewsCommentsDTO;
import com.bezkoder.springjwt.DTO.NewsDTO;
import com.bezkoder.springjwt.models.News;
import com.bezkoder.springjwt.models.NewsComments;
import com.bezkoder.springjwt.models.NewsLiked;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.repository.CommentService;
import com.bezkoder.springjwt.repository.CommentsRepository;
import com.bezkoder.springjwt.repository.INews;
import com.bezkoder.springjwt.repository.LikeRepository;
import com.bezkoder.springjwt.repository.LikeService;
import com.bezkoder.springjwt.repository.UserRepository;
import com.bezkoder.springjwt.sentiment_analysis.run;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/news")
public class NewsController {

	@Autowired
	INews newsService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	CommentsRepository commentRepository;

	@Autowired
	CommentService commentService;

	@Autowired
	LikeService likeService;

	@Autowired
	LikeRepository likeRepository;

	public static String url(String images, HttpServletRequest request) {
		String serverAddress = String.format("%s://%s:%d/", request.getScheme(), request.getServerName(),
				request.getServerPort());
		return serverAddress + images;
	}

	public static String convertDatetime(LocalDateTime datetime) {
//		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
//		String formatDateTime = datetime.format(format);
		DateTimeFormatter fmt = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
		return fmt.format(datetime);
	}

	@GetMapping(value = "")
	public Map<String, Object> getAll(HttpServletRequest request, Authentication authentication) {
		List<News> listNews = newsService.findAll();
		List<NewsDTO> output = new ArrayList<NewsDTO>();
		for (News news : listNews) {
			NewsDTO out = null;
			News newsT = newsService.findByIdNews(news.getId());
			User user = userRepository.findByusername(authentication.getName());
			Optional<NewsLiked> newsLiked = likeRepository.findByUserAndNews(user, newsT);
			boolean liked = false;
			if (newsLiked.isPresent()) {
				liked = true;
			}
			if (news.getImage() == null) {
				out = new NewsDTO(news.getId(), news.getTitle(), news.getContent(), news.getImage(), news.getLikes(),
						convertDatetime(news.getCreatedDate()), news.getStatus(), news.getCreatedBy().getUsername(),
						liked);
			} else {
				out = new NewsDTO(news.getId(), news.getTitle(), news.getContent(), url(news.getImage(), request),
						news.getLikes(), convertDatetime(news.getCreatedDate()), news.getStatus(),
						news.getCreatedBy().getUsername(), liked);
			}
			output.add(out);
		}
		Map<String, Object> results = new HashMap<String, Object>();
		results.put("results", output);
		return results;
	}

	@GetMapping(value = "/{id}")
	public NewsDTO getDetail(@PathVariable("id") Long id, HttpServletRequest request, Authentication authentication) {
		Optional<News> news = newsService.findById(id);
		NewsDTO result = null;
		News newsT = newsService.findByIdNews(id);
		User user = userRepository.findByusername(authentication.getName());
		Optional<NewsLiked> newsLiked = likeRepository.findByUserAndNews(user, newsT);
		boolean liked = false;
		if (newsLiked.isPresent()) {
			liked = true;
		}
		if (news.get().getImage() == null) {
			result = new NewsDTO(news.get().getId(), news.get().getTitle(), news.get().getContent(),
					news.get().getImage(), news.get().getLikes(), convertDatetime(news.get().getCreatedDate()),
					news.get().getStatus(), news.get().getCreatedBy().getUsername(), liked);
		} else {
			result = new NewsDTO(news.get().getId(), news.get().getTitle(), news.get().getContent(),
					url(news.get().getImage(), request), news.get().getLikes(),
					convertDatetime(news.get().getCreatedDate()), news.get().getStatus(),
					news.get().getCreatedBy().getUsername(), liked);
		}
		return result;
	}

//	Content-Type: application/json and Accept: application/json
//	produces = { MediaType.APPLICATION_JSON_VALUE }
//	consumes = {"multipart/mixed"}
	@PostMapping(value = "")
	public Map<String, Object> createNews(@RequestPart("title") String title, @RequestPart("content") String content,
			@RequestPart(value = "images", required = true) MultipartFile[] file, Authentication authentication,
			HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		News news = new News(title, content);
		boolean check = newsService.createNews(news, file, authentication.getName());
		result.put("success", check);
		return result;
	}

	@GetMapping(value = "/getByUser")
	public Map<String, Object> getByUserId(Authentication authentication, HttpServletRequest request) {
		Optional<User> user = userRepository.findByUsername(authentication.getName());
		List<News> listNews = newsService.findByCreatedBy(user.get().getId());
		List<NewsDTO> listNewsDTO = new ArrayList<NewsDTO>();
		for (News news : listNews) {
			NewsDTO newsDTO = new NewsDTO(news.getId(), news.getTitle(), news.getContent(),
					url(news.getImage(), request), news.getLikes(), convertDatetime(news.getCreatedDate()),
					news.getStatus(), news.getCreatedBy().getUsername());
			listNewsDTO.add(newsDTO);
		}
		Map<String, Object> results = new HashMap<String, Object>();
		results.put("results", listNewsDTO);
		return results;
	}

	@GetMapping(value = "/comments/{id}")
	public Map<String, Object> UserComment(@PathVariable("id") Long id) throws Exception {
//		try {
//			List<NewsComments> news = commentService.findById(id);
//			Map<String, Object> results = new HashMap<String, Object>();
//			results.put("success", true);
//			results.put("results", news);
//			return results;
//		} catch (Exception e) {
//			Map<String, Object> results = new HashMap<String, Object>();
//			results.put("success", false);
//			return results;
//		}
		List<NewsCommentsDTO> news = commentService.findById(id);
		Map<String, Object> results = new HashMap<String, Object>();
		results.put("success", true);
		results.put("results", news);
		return results;

	}

	@PostMapping(value = "/comments/{id}")
	public Map<String, Object> UserComment(@PathVariable("id") Long id, Authentication authentication,
			@RequestPart("comment") String comment) {
		try {
			News news = newsService.findByIdNews(id);
			if (comment == null || news == null) {
				Map<String, Object> results = new HashMap<String, Object>();
				results.put("success", false);
				return results;
			}
			User user = userRepository.findByusername(authentication.getName());
			NewsComments cmt = new NewsComments(comment, user, news, run.checkComment(comment));
			commentRepository.save(cmt);
			Map<String, Object> results = new HashMap<String, Object>();
			results.put("success", true);
			return results;
		} catch (Exception e) {
			Map<String, Object> results = new HashMap<String, Object>();
			results.put("success", false);
			return results;
		}
	}

	@GetMapping(value = "/like/{id}")
	public Map<String, Object> UserLike(@PathVariable("id") Long id) {
		try {
			List<NewsLiked> news = likeService.findById(id);
			Map<String, Object> results = new HashMap<String, Object>();
			results.put("success", true);
			results.put("like", news.size());
			return results;
		} catch (Exception e) {
			Map<String, Object> results = new HashMap<String, Object>();
			results.put("success", false);
			return results;
		}
	}

	@PostMapping(value = "/like/{id}")
	public Map<String, Object> UserLike(@PathVariable("id") Long id, Authentication authentication) {
		try {
			News news = newsService.findByIdNews(id);
			User user = userRepository.findByusername(authentication.getName());
			Optional<NewsLiked> liked = likeRepository.findByUserAndNews(user, news);
			if (liked.isPresent()) {
				NewsLiked newsLiked = new NewsLiked(liked.get().getId(), liked.get().getUser(), liked.get().getNews());
				likeRepository.delete(newsLiked);
				Map<String, Object> results = new HashMap<String, Object>();
				results.put("success", false);
				return results;
			}
			NewsLiked like = new NewsLiked(user, news);
			likeRepository.save(like);
			Map<String, Object> results = new HashMap<String, Object>();
			results.put("success", true);
			return results;
		} catch (Exception e) {
			Map<String, Object> results = new HashMap<String, Object>();
			results.put("success", false);
			return results;
		}
	}
}
