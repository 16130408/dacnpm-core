package com.bezkoder.springjwt.DTO;

import java.time.LocalDateTime;

import com.bezkoder.springjwt.models.User;

public class NewsDTO {
	private Long id;

	private String title;

	private String content;

	private String image;

	private int likes;

	private String createdDate;

	private boolean status;

	private String createdBy;

	private boolean liked;

	public NewsDTO(Long id, String title, String content, String image, int likes, String createdDate, boolean status,
			String createdBy) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.image = image;
		this.likes = likes;
		this.createdDate = createdDate;
		this.status = status;
		this.createdBy = createdBy;
	}

	public NewsDTO(Long id, String title, String content, String image, int likes, String createdDate, boolean status,
			String createdBy, boolean liked) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.image = image;
		this.likes = likes;
		this.createdDate = createdDate;
		this.status = status;
		this.createdBy = createdBy;
		this.liked = liked;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	@Override
	public String toString() {
		return "NewsDTO [id=" + id + ", title=" + title + ", content=" + content + ", image=" + image + ", likes="
				+ likes + ", createdDate=" + createdDate + ", status=" + status + ", createdBy=" + createdBy + "]";
	}

}
