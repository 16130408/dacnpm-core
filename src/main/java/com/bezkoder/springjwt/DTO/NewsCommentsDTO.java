package com.bezkoder.springjwt.DTO;

public class NewsCommentsDTO {
	private int id;
	private long news;
	private String comment;
	private String sentiment;
	private String user;

	public NewsCommentsDTO(int id, long news, String comment, String sentiment, String user) {
		this.id = id;
		this.news = news;
		this.comment = comment;
		this.sentiment = sentiment;
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getNews() {
		return news;
	}

	public void setNews(int news) {
		this.news = news;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "NewsCommentsDTO [id=" + id + ", news=" + news + ", comment=" + comment + "]";
	}

}
